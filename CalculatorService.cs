using Models;
using System;
using System.Xml.Linq;

public class CalculatorService : ICalculatorService
{
  public ResultModel Addition(RequestModel requestModel)
  {
    ResultModel AdditionResult = new ResultModel();
    AdditionResult.Result = requestModel.intA + requestModel.intB;
    return AdditionResult;
  }

  public ResultModel Subtraction(RequestModel requestModel)
  {
    ResultModel SubtractionResult = new ResultModel();
    SubtractionResult.Result = requestModel.intA - requestModel.intB;
    return SubtractionResult;
  }

  public ResultModel Multiplication(RequestModel requestModel)
  {
    ResultModel MultiplicationResult = new ResultModel();
    MultiplicationResult.Result = requestModel.intA * requestModel.intB;
    return MultiplicationResult;
  }

  public ResultModel Division(RequestModel requestModel)
  {
    ResultModel DivisionResult = new ResultModel();
    DivisionResult.Result = requestModel.intA / requestModel.intB;
    return DivisionResult;
  }
}