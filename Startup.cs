using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Models;
using SoapCore;

namespace otus_loadqa_soap
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // For SOAP services
            services.TryAddSingleton<ICalculatorService, CalculatorService>();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSoapEndpoint<ICalculatorService>("/Calculator.asmx", new BasicHttpBinding(), SoapSerializer.XmlSerializer);
        }
    }
}
