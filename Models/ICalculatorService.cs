using System.ServiceModel;

namespace Models
{
   [ServiceContract]
   public interface ICalculatorService
   {
      [OperationContract]
      ResultModel Addition(RequestModel requestModel);
      [OperationContract]
      ResultModel Subtraction(RequestModel requestModel);
      [OperationContract]
      ResultModel Multiplication(RequestModel requestModel);
      [OperationContract]
      ResultModel Division(RequestModel requestModel);
  }
}