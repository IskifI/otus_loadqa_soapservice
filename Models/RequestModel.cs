using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Models
{
    [DataContract]
    public class RequestModel
    {
       [DataMember]
       public int intA { get; set; }
       [DataMember]
       public int intB { get; set; }
    }
}