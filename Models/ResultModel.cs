using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Models
{
    [DataContract]
    public class ResultModel
    {
       [DataMember]
       public int Result { get; set; }
    }
}